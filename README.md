#android-automation-task-2-2

berberapa hal depedencies yang digunakan untuk ngerun automation JDK, Appium, Appium inspector, Git, android studio, selenium java, cucumber java dan appium java client


    1. JDK Java Development Kit (JDK) adalah lingkungan pengembangan perangkat lunak yang digunakan untuk mengembangkan aplikasi Java yang menggunakan proses pembuatan kode program Java. JDK berisi aplikasi Java Virtual Machine (JVM) dan Java Runtime Environment (JRE)

    2. appium sebagai tools untuk menguji aplikasi seluler dan dirancang untuk mengotomatisasi UI dari android dan iOS. Appium bekerja dengan mengirimkan perintah ke aplikasi mobile yang sedang diuji melalui WebDriver, sehingga pengembang dapat menguji aplikasi mobile pada berbagai jenis perangkat dan platform

    3. Appium Inspector adalah alat yang digunakan dalam pengujian dan pengembangan aplikasi seluler. yang berfungsi untuk Element Identification and Verification, Property Inspection, Code Generation, XPath and Accessibility ID Generation Appium Inspector menyediakan antarmuka pengguna grafis yang memudahkan untuk mengidentifikasi dan memverifikasi elemen seperti tombol, bidang teks, gambar, dan banyak lagi, memastikan pembuatan skrip otomatisasi yang pas dan akurat 

    4. GIT juga dikenal dengan istilah Distributed Revision Control yang artinya adalah sebuah penyimpanan database GIT yang tidak hanya pada satu tempat saja. Bisa menyimpan seluruh versi source code Bisa paham cara kolaborasi dalam proyek Bisa ikut berkontribusi ke poryek open-source; Lebih aman digunakan untuk kolaborasi, karena kita bisa tahu apa yang diubah dan siapa yang mengubahnya. Bisa memahami cara deploy aplikasi modern Bisa membuat blog dengan SSG.

    5. Android Studio  adalah IDE yang lengkap dan powerful untuk pengembangan aplikasi Android. IDE ini menawarkan berbagai fitur dan alat yang memudahkan dan mempercepat proses pengembangan aplikasi Android. Android studio digunakan untuk membuat virtual device yang dibutuhkan agar bisa dijalankan yang mewakili perangkat Android tertentu. avd dapat menggunakan emulator Android sebagai platform target untuk menjalankan dan menguji aplikasi Android

    6. selenium java adalah tools pengujian aplikasi yang open source dan otomatis. Dengan Selenium, Tester dapat menguji berbagai aplikasi web lintas browser dan platform secara otomatis selenium juga menyediakan library selenium yang diperlukan berinteraksi dengan browser web.

    7. cucumber java  digunakan untuk menjalankan Automation Acceptance Tests based on BDD(Behaviour Driven Development). Hasil eksekusi dari Cucumber akan diperlihatkan secara ‘lebih manusiawi’, karena hasil test yang diperlihatkan adalah ‘scenario’ yang telah dibuat. 

    8. appium java client library Java yang digunakan untuk mengotomatisasi pengujian aplikasi mobile menggunakan Appium.Dependencies ini menyediakan pustaka-pustaka yang diperlukan untuk berinteraksi dengan Appium dalam bahasa Java.

Setup yang dilakukan untuk menjalankan automation test:

    JDK 
    1. https://www.oracle.com/java/technologies/javase/jdk17-archive-downloads.html 
    2. pilih download sesuai platfrom masing masing (windows atau mac)
    3. setelah terinstall, tambahkan JAVA_HOME ke system variable
    4. lalu, tambahkan direktori bin JDK ke variabel PATH pada system variable

    appium 
    1. buka terminal
    2. Install node brew install node ketik "appium" lalu enter
    3. Appium akan berjalan

    android studio
    1. install android studio
    2. buat virtual device yang ingin dijalankan dengan device adb agar bisa melakukan debugging
    3. jalankan emulator

    appium inspector
    1. jalankan appium
    2. install appium inspector
    3. buka virtual device melalui android studio
    4. masukan capicity builder sesuai kebutuhan
    5. Setelah server berjalan, dapat membuka Appium Inspector baik melalui aplikasi Appium Desktop atau dengan mengakses URL Inspector yang disediakan oleh server.

    intellij
    1. buka intellij dengan format gradle, grovvy dan JDK 17
    2. build gradle yang digunakan gradle 8.3 dengan format gradler-wrapper.properties sebagai berikut : Pada gradle-wrapper.properties diganti dengan kode
distributionBase=GRADLE_USER_HOME
distributionPath=wrapper/dists
distributionUrl=https://services.gradle.org/distributions/gradle-8.3-bin.zip
zipStoreBase=GRADLE_USER_HOME
zipStorePath=wrapper/dists


    

