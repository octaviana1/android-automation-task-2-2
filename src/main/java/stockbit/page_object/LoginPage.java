package stockbit.page_object;

import org.openqa.selenium.By;

public class LoginPage extends BasePageObject {
    public void isOnboardingPage() {
        assertIsDisplay("ICON_STOCKBIT");
    }

    private void assertIsDisplay(String iconStockbit) {
    }

    public void tapLogin()  {
        tap("BUTTON_LOGIN");
    }

    public void inputUsername(String username) {
        typeOn(By.xpath("(//android.widget.EditText[@resource-id='com.stockbit.android:id/tiet_text_field_input' and @text='Username or Email'])"), env(username));
    }

    private String env(String username) {
        return username;
    }

    public void inputPassword (String password) {
        typeOn(By.xpath("(//android.widget.EditText[@resource-id='com.stockbit.android:id/tiet_text_field_input' and @text='Password'])"), env(password));
    }

    public void tapLoginButton()  {
        tap("BUTTON_ICON_LOGIN");
    }

    public void tapSkipBiometricPopup()  {
        tap("BUTTON_SKIP_SMART_LOGIN");
    }

    public void tapSkipAvatarPopup()  {
        tap("BUTTON_SKIP_CHOOSE_AVATAR");
    }

    public void isWatchlistPage()  {
        tapSkipBiometricPopup();
        tapSkipAvatarPopup();
        assertIsDisplay("ALL_WHATCHLIST=");
    }
}